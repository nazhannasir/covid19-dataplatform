<script>
    
    var sliderValue = [];
    
    var listDate = [];
    var startDate ='2020-01-25';
    var endDate = '2020-03-31';
    var dateMove = new Date(startDate);
    var strDate = startDate;

    while (strDate < endDate){
      var strDate = dateMove.toISOString().slice(0,10);
      sliderValue.push(strDate);
      dateMove.setDate(dateMove.getDate()+1);
    };

    document.querySelector("#myRange").setAttribute("max",sliderValue.length-1)
    // here we have set max attribute to one less array length i.e. 7-1 = 6

    const settings={
      fill: '#fa697c',
      background: '#d7dcdf'
    } // colour settings for slider fill effect
    var date_chosen=startDate;

    first_date = sliderValue[0];
    document.querySelector('span').innerHTML = `${first_date.substr(8,2)}/${first_date.substr(5,2)}/${first_date.substr(0,4)}`
    // intialized slider value to array first element.

    // below code execute when we move the slider 
    document.querySelector("#myRange").addEventListener('input', (event)=>{
        date_selected=sliderValue[parseInt(event.target.value)];

    // 1. apply our value to the span
        document.querySelector('span').innerHTML = `${date_selected.substr(8,2)}/${date_selected.substr(5,2)}/${date_selected.substr(0,4)}`;

        // 2. apply our fill to the input
        // applyFill(event.target);

        date_chosen=date_selected;
        generate_map(date_chosen);
    });

    // This function applies the fill to our sliders by using a linear gradient background
    // function applyFill(slider) {
    //    const percentage = 100*(slider.value-slider.min)/(slider.max-slider.min);
    //    const bg = `linear-gradient(90deg, ${settings.fill} ${percentage}%, ${settings.background} ${percentage+0.1}%)`;
    //    slider.style.background = bg;
    // }

    var map = L.map('mapid').on('dragend', function(){}).setView([3.067842, 101.689303], 15);;

    var generate_map = (date_chosen) => {
        // alert();

        map.remove();
        map = L.map('mapid').on('dragend', function(){}).setView([3.067842, 101.689303], 15);

        // map.eachLayer(function(layer){
        //     map.remove();
        //     map.removeLayer(layer);
        //     map.removeLayer(line);
        //     latlngs = [];
        //     PointManager.deletePoints(); //clear the pointManager array of points
        //     isMap = false;
        //     loadMap(); //reload the map function
        // });

        var pjson=[]
        // without time
        for(const key in markers) {
            // id
            for(const k2 in markers[key]) {
                id = k2;
                // date
                for(const k3 in markers[key][k2]) {
                    // console.log(k3, date_chosen);
                    if (k3 == date_chosen){

                        // console.log(pjson);
                        lat=markers[key][k2][k3][0].lat;
                        long=markers[key][k2][k3][0].long;
                        state=markers[key][k2][k3][0].state;

                        pjson.push([lat, long, state, id]);

                    }
                    // console.log(markers[key][k2][k3]);
                    
                    // console.log(pjson);
                    // L.marker([lat, long], {icon: myIcon}).addTo( mymap);
                }
            }
        }

        // var map = L.map('mapid');
        pjson.forEach(function(lngLat) {
          L.marker(lngLatToLatLng(lngLat)).bindPopup( 'ID: ' + lngLat[3]).addTo(map);
        })

        var polyline = L.polyline(lngLatArrayToLatLng(pjson));

        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',maxZoom: 18,
        })
        .addTo(map);

        map.fitBounds(polyline.getBounds());
    }
    

    function lngLatArrayToLatLng(lngLatArray) {
      return lngLatArray.map(lngLatToLatLng);
    }

    function lngLatToLatLng(lngLat) {
      return [lngLat[0], lngLat[1]];
    }

    generate_map(startDate);


    // const myCustomColour = '#583470'
    // const markerHtmlStyles = `
    //   background-color: ${myCustomColour};
    //   width: 2rem;
    //   height: 2rem;
    //   display: block;
    //   left: -1.5rem;
    //   top: -1.5rem;
    //   position: relative;
    //   border-radius: 3rem 3rem 0;
    //   transform: rotate(45deg);
    //   border: 1px solid #FFFFFF`

    // const myIcon = L.divIcon({
    //   className: "my-custom-pin",
    //   iconAnchor: [0, 24],
    //   labelAnchor: [-6, 0],
    //   popupAnchor: [0, -36],
    //   html: `<span style="${markerHtmlStyles}" />`
    // })





    // var marker = L.marker([3.067842, 101.689303]).addTo(mymap);
    // var marker = L.marker([98.84730671644503, 46.328557924429]).addTo(mymap);

    // var circle = L.circle([3.067842, 101.693303], {
    //                 color: 'red',
    //                 fillColor: '#f03',
    //                 fillOpacity: 0.5,
    //                 radius: 200
    //             }).addTo(mymap);


    // for ( var i=0; i < markers.length; ++i )
    // {
    //     // for (){

    //     // }
    //      L.marker( [markers[i].lat, markers[i].lng], {icon: myIcon} )
    //       // .bindPopup( '<a href="' + markers[i].url + '" target="_blank">' + markers[i].name + '</a>' )
    //       .addTo( map );
    // }
    var lat='';
    var long='';

    // with time
    // for(const key in markers) {
    //     // id
    //     for(const k2 in markers[key]) {
    //         // date
    //         for(const k3 in markers[key][k2]) {
    //             for(const k4 in markers[key][k2][k3]) {
    //                 lat=markers[key][k2][k3][k4].lat;
    //                 long=markers[key][k2][k3][k4].long;
    //                 L.marker([lat, long]).addTo( mymap );

    //             }
    //         }
    //     }
    // }

    


    



    // Example POST method implementation:
// async function postData(url = '', data = {}) {
//     // Default options are marked with *
//     const response = await fetch(url, {
//         method: 'POST', // *GET, POST, PUT, DELETE, etc.
//         mode: 'cors', // no-cors, *cors, same-origin
//         cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
//         credentials: 'same-origin', // include, *same-origin, omit
//         headers: {
//         'Content-Type': 'application/json'
//         // 'Content-Type': 'application/x-www-form-urlencoded',
//         },
//         redirect: 'follow', // manual, *follow, error
//         referrerPolicy: 'no-referrer', // no-referrer, *client
//         body: JSON.stringify(data) // body data type must match "Content-Type" header
//     });
//     return await response.json(); // parses JSON response into native JavaScript objects
//     }


</script>