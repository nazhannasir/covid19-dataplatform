@include('partials.header')
<div class="container content">
    @yield('content')
</div>
@include('partials.footer')