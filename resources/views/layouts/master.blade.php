<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        @include('partials.style')
    </head>
    <body>
        @include('partials.body')
    </body>

    @include('partials.script')
    @include('partials.datascript')
    @include('partials.afterscript')
    
</html>
