<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cases;

class LocationController extends Controller
{
    public function getLocation(Request $request){
        $data = Cases::whereBetween('latitude', [$request->point1[0], $request->point2[0]])->whereBetween('longitude', [$request->point1[1],$request->point2[1]])->get()->toJson();
        
        return response()->json(['success' => 'display data', 'data' => $data]);
    }
}
